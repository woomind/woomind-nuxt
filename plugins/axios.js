import https from 'https'

/**
 * Register an interceptor for axios under the form of a plugin to add credentials to every request made by $axios.
 */
export default function (ctx) {
  // In development, let's ignore self-encrypted certificates.
  if (process.env.NODE_ENV === 'development') {
    ctx.$axios.defaults.httpsAgent = new https.Agent({
      rejectUnauthorized: false,
    })
  }

  /**
   * Change technical error answered from server to user comprehensible answers.
   */
  ctx.$axios.onError((error) => {
    // If there is no answer at all : we can assume there is a network error.
    if (error.response === undefined) {
      error.name = 'Network Error'
      error.message = 'Please retry later.'
    } else {
      // Generically transform the response.
      if (error.response.data.error) {
        const response = error.response.data.error
        if (response.type) {
          error.name = response.message
          error.message = response.description
          error.fields = response.fields
        }
      }

      if (error.response.status === 500 || error.response.status === 404) {
        error.name = 'Oups... something went wrong'
        error.message = 'Please contact use, we will fix it!'
      } else if (
        error.response.status === 401 ||
        error.response.status === 403
      ) {
        // If we are not already on login page : logout.
        if (!location.pathname.startsWith(ctx.$auth.options.redirect.login)) {
          // Dispatch logout action : we have some customs to do when logout happens.
          // ctx.store.dispatch('logout')
          // We do not use logout() to not run revoke token.
          // Indeed, we would not be able to revoke a token for which we are not authorized, thus leading to 401 and infinite loop.
          // ctx.$auth.reset()
          // Route the user to login page.
          // await ctx.$router.push('/login')
          // Notify
          error.name = 'Permission denied'
          error.message = "You don't have permission for this operation."
        } else {
          error.name = 'Authentication failed'
          error.message = 'Your email or password is incorrect.'
        }
      }
    }

    ctx.store.commit('snackbar/showMessage', {
      title: error.name,
      message: error.message,
    })
  })
}
