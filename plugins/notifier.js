export default ({ app, store }, inject) => {
  inject('notifier', {
    showMessage({ title = '', message = '', color = 'info' }) {
      store.commit('snackbar/showMessage', { title, message, color })
    }
  })
}
