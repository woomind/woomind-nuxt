/**
 * Registers new login / logout methods to $auth service overriding old ones.
 *
 * $auth module have issues redirecting on SSR mode nuxt app., therefore we
 * explicitely set rediction here.
 *
 * @param $auth
 * @param store
 */
export default function ({ $auth, store }) {
  const oldLogout = $auth.logout.bind($auth)
  const oldLogin = $auth.login.bind($auth)

  $auth.logout = async (...args) => {
    const _ = oldLogout(...args)
    _.then(() => {
      $auth.redirect('logout')
    })
    return _
  }

  $auth.login = (...args) => {
    const _ = oldLogin(...args)
    _.then(() => {
      $auth.redirect('home')
    })
    return _
  }
}
