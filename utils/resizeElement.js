/**
 * This methods looks at the elements within given 'el' reference and calculates
 * how many childs can fit.
 *
 * This is specially usefull for displaying the tags as chip currently.
 * @param elWrapper
 * @returns {number}
 */
export default function(elWrapper) {
  // Unexpected wrapper.
  if (!elWrapper) {
    return
  }

  const elChildren = elWrapper.$children
  // If the chips wrapper has children.
  if (elChildren.length) {
    // console.log('count of tags:', elChildren.length)
    // console.log('onResize', elWrapper.$el.clientWidth)
    // Get the container size.
    const containerSize = elWrapper.$el.clientWidth
    // Currently nothing is included.
    let includedChipsCount = 0
    let includedChipsSize = 120 // Leave room for the chips counter.

    // While there are children and we have room in the container.
    while (
      elChildren[includedChipsCount] &&
      '$el' in elChildren[includedChipsCount] &&
      includedChipsSize < containerSize
    ) {
      // We need a little trick: we must ensure the chip is visible so we can compute it's size.
      const originalDisplay = elChildren[includedChipsCount].$el.style.display
      elChildren[includedChipsCount].$el.style.display = 'inline-block'
      // console.log(`Chip ${includedChipsCount} size is ${elChildren[includedChipsCount].$el.clientWidth} (${includedChipsSize})`)
      includedChipsSize += elChildren[includedChipsCount].$el.clientWidth + 5
      // Then we can set it's display back and move to next.
      elChildren[includedChipsCount].$el.style.display = originalDisplay
      includedChipsCount++
    }
    // Trick here: because of previous loop, we are "1" above the number that fits.
    // Plus, the index start at 1 in v-for and at 0 in array here, so we start 1 too far.
    // Hence: remove 2.
    includedChipsCount = Math.max(0, includedChipsCount)
    // console.log('fitting elements:', includedChipsCount)
    return includedChipsCount
  }
}
