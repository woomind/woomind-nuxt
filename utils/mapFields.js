export default function(fields) {
  const computeds = {}

  for (const field of fields) {
    computeds[field] = {
      get() {
        return this.bookmark[field]
      },
      set(value) {
        this.$store.commit('bookmarks/setDialog', {
          type: 'edit',
          bookmark: { ...this.bookmark, [field]: value }
        })
      }
    }
  }

  return computeds
}
