/**
 * Blurs every field children of the given reference.
 *
 * @param el
 */
export default function(el) {
  el.$children.forEach((item) => {
    if (typeof item.blur === 'function') {
      item.blur()
    }
  })
}
