/**
 * Contains access to client trusted API endpoints.

 *
 * This is done because Vue.js runs server-side while our OAuth server uses
 * password strategy for trusted clients like ours. Therefore it requires
 * confidential infos (like client_secret) we cannot expose on client side.
 */
const https = require('https')
const axios = require('axios')
const btoa = require('btoa')
const express = require('express')
const router = express.Router()

// In development, let's ignore self-encrypted certificates.
if (process.env.NODE_ENV === 'development') {
  axios.defaults.httpsAgent = new https.Agent({ rejectUnauthorized: false })
}

/**
 * Proxies the /oauth/token call.
 */
router.post('/login', function (req, res) {
  const basicAuth =
    'Basic ' +
    btoa(
      process.env.WOOMIND_CLIENT_ID + ':' + process.env.WOOMIND_CLIENT_SECRET
    )
  axios
    .request({
      method: 'post',
      url: process.env.WOOMIND_API_URL + '/oauth/token',
      data: {
        grant_type: 'password',
        response_type: 'token',
        username: req.body.email, // Serverside we are using oauth. Hence, our use generic username key here.
        password: req.body.password,
        scope: 'user:all bookmark:all',
      },
      headers: {
        Accept: 'application/json',
        Authorization: basicAuth,
      },
    })
    .then((response) => {
      res.send(JSON.stringify(response.data))
    })
    .catch((error) => {
      console.log(error)
      res.status(error.response ? error.response.status : 400)
      res.send(error.response ? error.response.data : error)
    })
})

/**
 * Proxies the /v2/users/register call.
 */
router.post('/register', function (req, res) {
  const basicAuth =
    'Basic ' +
    btoa(
      process.env.WOOMIND_CLIENT_ID + ':' + process.env.WOOMIND_CLIENT_SECRET
    )
  axios
    .request({
      method: 'post',
      url: process.env.WOOMIND_API_URL + '/users/register',
      data: {
        email: req.body.email,
        password: req.body.password,
      },
      headers: {
        Accept: 'application/json',
        Authorization: basicAuth,
      },
    })
    .then((response) => {
      res.send(JSON.stringify(response.data))
    })
    .catch((error) => {
      res.status(error.response.status)
      res.send(error.response.data)
    })
})

module.exports = router
