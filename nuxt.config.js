const fs = require('fs')
require('dotenv').config()
const colors = require('vuetify/es5/util/colors').default

module.exports = {
  mode: 'universal',
  /**
   * Server's configuration.
   */
  server: {
    https: {
      key: fs.readFileSync('./certfiles/privatekey.pem'),
      cert: fs.readFileSync('./certfiles/certificate.pem'),
    },
  },
  /**
   * Defines env variables for client side.
   */
  env: {
    API_URL: process.env.WOOMIND_API_URL,
    API_CLIENT_ID: process.env.WOOMIND_CLIENT_ID,
  },
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: 'WooMind - Bookmark everything !' || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['~/assets/styles.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/axios', '~/plugins/notifier'],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    // '@nuxtjs/stylelint-module',
    '@nuxtjs/vuetify',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://auth.nuxtjs.org
    '@nuxtjs/auth',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://pwa.nuxtjs.org/
    '@nuxtjs/pwa',
  ],
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      light: true,
      themes: {
        light: {
          primary: colors.blue, // #2196f3
          secondary: colors.pink.accent3,
          error: colors.red.base,
          disabled: colors.grey,
        },
      },
    },
  },
  /*
   ** Axios module configurations
   */
  axios: {
    baseURL: '/',
  },
  /*
   ** Auth module configuration
   */
  auth: {
    plugins: ['~/plugins/auth.js'],
    redirect: {
      home: '/dashboard',
      login: '/login',
      callback: '/callback',
      logout: '/',
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/proxy/auth/login',
            method: 'post',
            propertyName: 'access_token',
          },
          logout: {
            url: `${process.env.WOOMIND_API_URL}/oauth/revoke`,
            method: 'get',
          },
          user: {
            url: `${process.env.WOOMIND_API_URL}/users/me`,
            method: 'get',
            propertyName: 'data',
          },
        },
        tokenRequired: true,
        tokenType: 'Bearer',
      },
    },
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },
}
