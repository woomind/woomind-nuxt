export const state = () => ({
  list: [], // Stores the list of remotely fetch bookmarks.
  operations: {}, // Stores ongoing operations on bookmarks.
  dialog: {
    // Represents the currently open dialogbox.
    type: null,
    bookmark: null,
  },
  displayType: 'card',
})

export const mutations = {
  /** Add a single bookmark on top of current list */
  addBookmark: (state, bookmark) => {
    state.list = [bookmark, ...state.list]
  },
  /** Add multiple bookmarks on top of current list */
  addBookmarks: (state, bookmarks) => {
    state.list = [...bookmarks, ...state.list]
  },
  /** Update given bookmark in the current list */
  setBookmark: (state, bookmark) => {
    state.list = state.list.map((item) =>
      item.bid !== bookmark.bid ? item : bookmark
    )
  },
  /** Replace all bookmarks with given onces. */
  setBookmarks: (state, bookmarks) => {
    state.list = bookmarks
  },
  /** Remove given bookmark in the current list */
  unsetBookmark: (state, bookmark) => {
    state.list = state.list.filter((item) => item.bid !== bookmark.bid)
  },
  /** Set indicator of current operation (one per bookmark only) */
  setOperation: (state, { target, operation }) => {
    state.operations = { ...state.operations, [target]: operation }
  },
  /** Cancel current operation on given bookmark */
  cancelOperation: (state, target) => {
    // @todo: understand this. Those two lines seems to be redundant.
    // But the first one triggers reactiveness in store to page level components (such as CreateAction)
    // The second triggers full reactiveness on the rest.
    state.operations = { ...state.operations, [target]: null }
    // Not yet sure why only this is not enough...
    delete state.operations[target]
  },
  // Closes the current dialog form (one only).
  setDialog: (state, { type, bookmark }) => {
    state.dialog = { type, bookmark }
  },
  // Closes the current dialogbox.
  cancelDialog: (state) => {
    state.dialog = { type: null, bookmark: null }
  },
  setDisplayType: (state, mode) => {
    state.displayType = mode
  },
}

export const actions = {
  /** Remote fetch all available bookmarks */
  async fetch({ commit }, payload) {
    commit('setOperation', {
      target: 'fetch',
      operation: 'fetch',
    })
    try {
      const params = { search: this.$router.currentRoute.query.search }
      const { data } = await this.$axios.get(
        `${process.env.API_URL}/bookmarks/me`,
        {
          params,
        }
      )
      commit('setBookmarks', data.data)
    } catch (err) {}
    commit('cancelOperation', 'fetch')
  },
  /** Remote create a bookmark */
  async create({ commit }, bookmark) {
    commit('setOperation', {
      target: 'create',
      operation: 'create',
    })
    try {
      const { data } = await this.$axios.post(
        `${process.env.API_URL}/bookmarks/me`,
        bookmark
      )
      commit('addBookmark', data.data)
      this.$notifier.showMessage({
        message: 'Bookmark has been added on top of the list.',
      })
    } catch (err) {}
    commit('cancelOperation', 'create')
  },
  /** Remote update a bookmark */
  async update({ commit }, { bid, payload, operation }) {
    commit('setOperation', {
      target: bid,
      operation: operation || 'update',
    })
    try {
      const { data } = await this.$axios.patch(
        `${process.env.API_URL}/bookmarks/${bid}/me`,
        payload
      )
      commit('setBookmark', data.data)
      this.$notifier.showMessage({
        message: 'Bookmark has been updated.',
      })
    } catch (err) {}
    commit('cancelOperation', bid)
  },
  /** Remote delete a bookmark */
  async remove({ commit }, bookmark) {
    commit('setOperation', {
      target: bookmark.bid,
      operation: 'remove',
    })
    try {
      const { data } = await this.$axios.delete(
        `${process.env.API_URL}/bookmarks/${bookmark.bid}/me`
      )
      commit('unsetBookmark', data.data)
      this.$notifier.showMessage({
        message: 'Bookmark has been deleted.',
      })
    } catch (err) {}
    commit('cancelOperation', bookmark.bid)
  },
  /** Import bookmarks */
  async import({ commit }, formData) {
    commit('setOperation', {
      target: 'import',
      operation: {
        loading: true,
        token: 123,
      },
    })
    try {
      formData.append(
        'callback',
        `${process.env.BASE_URL}/ping/callback?operation=import&token=123`
      )
      const { data } = await this.$axios.post(
        `${process.env.API_URL}/bookmarks/import/me`,
        formData
      )
      commit('setOperation', {
        target: 'import',
        operation: {
          loading: false,
          ...data,
        },
      })
    } catch (e) {
      commit('cancelOperation', 'import')
    }
  },
}
