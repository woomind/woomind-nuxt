export const state = () => ({
  title: '',
  message: '',
  color: ''
})

export const mutations = {
  showMessage(state, payload) {
    state.title = payload.title
    state.message = payload.message
    state.color = payload.color
  }
}
